package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.Account;

import java.util.List;

public interface AccountService {
    List<Account> getAllAccount();

    Account saveAccount(Account account) throws Exception;
}
