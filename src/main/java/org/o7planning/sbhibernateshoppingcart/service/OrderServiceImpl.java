package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.Account;
import org.o7planning.sbhibernateshoppingcart.entities.OrderDetail;
import org.o7planning.sbhibernateshoppingcart.entities.Orders;
import org.o7planning.sbhibernateshoppingcart.entities.Product;
import org.o7planning.sbhibernateshoppingcart.model.CartInfo;
import org.o7planning.sbhibernateshoppingcart.model.CartLineInfo;
import org.o7planning.sbhibernateshoppingcart.repository.OrderDetailRepository;
import org.o7planning.sbhibernateshoppingcart.repository.OrderRepository;
import org.o7planning.sbhibernateshoppingcart.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderSerive {
    private final OrderRepository orderRepository;

    private final OrderDetailRepository orderDetailRepository;

    private final ProductRepository productRepository;

    public OrderServiceImpl(OrderRepository orderRepository, OrderDetailRepository orderDetailRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.productRepository = productRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Orders save(CartInfo cartInfo, Account account) {
        Orders orders = Orders.builder()
                .amount(cartInfo.getAmountTotal())
                .orderDate(new Date())
                .customerAddress(cartInfo.getCustomerInfo().getAddress())
                .customerPhone(cartInfo.getCustomerInfo().getPhone())
                .customerName(account.getUserName())
                .build();
        Orders save = orderRepository.save(orders);

        for (CartLineInfo cartLineInfo : cartInfo.getCartLines()) {
            Product product = productRepository.findById(cartLineInfo.getProductInfo().getCode()).get();
            OrderDetail orderDetail = new OrderDetail(null, orders, product, cartLineInfo.getQuantity(), cartLineInfo.getProductInfo().getPrice(), cartLineInfo.getAmount());
            orderDetailRepository.save(orderDetail);
        }
        return save;
    }

    @Override
    public List<Orders> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public List<Orders> findAllByUsername(String customerName) {
        return orderRepository.findAllByCustomerName(customerName);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Orders saveOrders(Orders orders) {
        orders.setOrderDate(new Date());
        return orderRepository.save(orders);
    }
}
