package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.Account;
import org.o7planning.sbhibernateshoppingcart.entities.Orders;
import org.o7planning.sbhibernateshoppingcart.model.CartInfo;

import java.util.List;

public interface OrderSerive {
    Orders save(CartInfo cartInfo, Account account) throws Exception;

    List<Orders> getAllOrders();

    List<Orders> findAllByUsername(String customerName);

    Orders saveOrders(Orders orders) throws Exception;
}
