package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.OrderDetail;
import org.o7planning.sbhibernateshoppingcart.repository.OrderDetailRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    private final OrderDetailRepository orderDetailRepository;

    public OrderDetailServiceImpl(OrderDetailRepository orderDetailRepository) {
        this.orderDetailRepository = orderDetailRepository;
    }

    @Override
    public List<OrderDetail> findOrders(Long id) {
        return orderDetailRepository.findAllByOrdersId(id);
    }
}
