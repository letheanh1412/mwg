package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll(String status);
    Product findProduct(Long code);
    Product saveProduct(Product product) throws Exception;
    Boolean removeProduct(Long code) throws Exception;
}
