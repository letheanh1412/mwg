package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.Product;
import org.o7planning.sbhibernateshoppingcart.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAll(String status) {
        return productRepository.findAllByStatus("Active");
    }

    @Override
    public Product findProduct(Long code) {
        return productRepository.findById(code).get();
    }

    @Transactional(rollbackFor = Exception.class)
    public Product saveProduct(Product product) {
        product.setCreatDate(new Date());
        return productRepository.save(product);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeProduct(Long code) {
        Product product = productRepository.findById(code).get();
        try {
            product.setStatus("DeActive");
            productRepository.save(product);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
