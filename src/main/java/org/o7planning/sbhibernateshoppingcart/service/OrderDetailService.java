package org.o7planning.sbhibernateshoppingcart.service;

import org.o7planning.sbhibernateshoppingcart.entities.OrderDetail;

import java.util.List;

public interface OrderDetailService {
    List<OrderDetail> findOrders(Long id);
}
