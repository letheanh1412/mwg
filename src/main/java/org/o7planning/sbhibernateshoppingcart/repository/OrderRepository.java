package org.o7planning.sbhibernateshoppingcart.repository;

import org.o7planning.sbhibernateshoppingcart.entities.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long> {
    List<Orders> findAllByCustomerName(String userName);
}
