package org.o7planning.sbhibernateshoppingcart.repository;

import org.o7planning.sbhibernateshoppingcart.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {
    Account findByUserName(String userName);
}
