package org.o7planning.sbhibernateshoppingcart.repository;

import org.o7planning.sbhibernateshoppingcart.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByStatus(String status);
}
