package org.o7planning.sbhibernateshoppingcart.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductForm {
    private Long code;
    private String name;
    private Double price;
    private boolean newProduct = false;
    private MultipartFile multipartFile;
}
