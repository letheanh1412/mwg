package org.o7planning.sbhibernateshoppingcart.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders")
public class Orders implements Serializable {
    private static final long serialVersionUID = -2576670215015463100L;
    @Id
    @Column(name = "id", length = 50)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_date", nullable = false)
    private Date orderDate;

    @Column(name = "amount", nullable = false)
    private double amount;

    @Column(name = "customer_name", length = 255, nullable = true)
    private String customerName;

    @Column(name = "customer_address", length = 255, nullable = true)
    private String customerAddress;

    @Column(name = "customer_email", length = 128, nullable = true)
    private String customerEmail;

    @Column(name = "customer_phone", length = 128, nullable = true)
    private String customerPhone;
}
