package org.o7planning.sbhibernateshoppingcart.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "accounts")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    private static final long serialVersionUID = -2054386655979281969L;
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";

    @Id
    @Column(name = "user_name", length = 20, nullable = false, unique = true)
    private String userName;

    @Column(name = "encryted_password", length = 128, nullable = false)
    private String encrytedPassword;

    @Column(name = "active", length = 1, nullable = false)
    private boolean active;

    @Column(name = "user_role", length = 20, nullable = false)
    private String userRole;

    @Column(name = "mser_mail", length = 255, nullable = true)
    private String userMail;

    @Column(name = "user_phone", length = 20, nullable = true)
    private String userPhone;

    @Column(name = "user_address", length = 255, nullable = true)
    private String userAddress;
}
