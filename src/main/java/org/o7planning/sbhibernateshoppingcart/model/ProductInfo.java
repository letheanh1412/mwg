package org.o7planning.sbhibernateshoppingcart.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.o7planning.sbhibernateshoppingcart.entities.Product;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductInfo {
    private Long code;
    private String name;
    private String image;
    private double price;

    public ProductInfo(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.image = product.getImage();
    }
}
