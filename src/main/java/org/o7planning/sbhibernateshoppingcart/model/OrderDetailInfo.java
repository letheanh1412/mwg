package org.o7planning.sbhibernateshoppingcart.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailInfo {
    private Long id;
    private String productCode;
    private String productName;
    private int quanity;
    private double price;
    private double amount;
}
