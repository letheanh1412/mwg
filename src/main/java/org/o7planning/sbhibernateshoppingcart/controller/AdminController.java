package org.o7planning.sbhibernateshoppingcart.controller;

import org.o7planning.sbhibernateshoppingcart.entities.Product;
import org.o7planning.sbhibernateshoppingcart.model.OrderDetailInfo;
import org.o7planning.sbhibernateshoppingcart.model.ProductInfo;
import org.o7planning.sbhibernateshoppingcart.service.AccountService;
import org.o7planning.sbhibernateshoppingcart.service.OrderDetailService;
import org.o7planning.sbhibernateshoppingcart.service.OrderSerive;
import org.o7planning.sbhibernateshoppingcart.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

@Controller
public class AdminController {
    private final ProductService productService;

    private final AccountService accountService;

    private final OrderSerive orderSerive;

    private final OrderDetailService orderDetailService;

    public AdminController(ProductService productService, AccountService accountService, OrderSerive orderSerive, OrderDetailService orderDetailService) {
        this.productService = productService;
        this.accountService = accountService;
        this.orderSerive = orderSerive;
        this.orderDetailService = orderDetailService;
    }

    @GetMapping("/admin")
    public String admin(HttpSession httpSession, Model model) {
        if (httpSession.getAttribute("myProduct") == null) {
            httpSession.setAttribute("myProduct", new ProductInfo());
        }
        model.addAttribute("listProductInAdmin", productService.getAll("Active"));
        return "admin/admin";
    }

    @GetMapping("/addProduct")
    public String addProduct(Model model) {
        model.addAttribute("Product", new Product());
        return "admin/addProduct";
    }

    @PostMapping("/doneAddProduct")
    public String doneAddProduct(Model model, @ModelAttribute Product product) throws Exception {
        product.setStatus("Active");
        Product addProduct = productService.saveProduct(product);
        model.addAttribute("Product", addProduct);
        return "redirect:/admin";
    }

    @GetMapping("/update/product/{code}")
    public String updateProduct(Model model, @PathVariable Long code) {
        model.addAttribute("Product", productService.findProduct(code));
        return "admin/updateProduct";
    }

    @PostMapping("/update/product")
    public String doneUpdateProduct(Model model, @ModelAttribute Product product) throws Exception {
        product.setStatus("Active");
        Product addProduct = productService.saveProduct(product);
        model.addAttribute("Product", addProduct);
        return "redirect:/admin";
    }

    @GetMapping("/remove/product/{code}")
    public String doneRemoveProduct(@PathVariable Long code) {
        try {
            productService.removeProduct(code);
            return "redirect:/admin";
        } catch (Exception e) {
            return "redirect:/admin";
        }
    }

    @GetMapping("/403")
    public String error403() {
        return "/403";
    }

    @GetMapping("/showReceipt")
    public String showReciept(HttpSession httpSession, Model model) {
        if (httpSession.getAttribute("myOrderDetail") == null) {
            httpSession.setAttribute("myOrderDetail", new OrderDetailInfo());
        }
        model.addAttribute("listReciepts", orderSerive.getAllOrders());
        return "admin/showReceipt";
    }

    @GetMapping("/recieptDetail/{id}")
    public String recieptDetail(HttpSession httpSession, Model model, @PathVariable Long id) {
        model.addAttribute("listRecieptDetail", orderDetailService.findOrders(id));
        if (httpSession.getAttribute("myOrderDetail") == null) {
            httpSession.setAttribute("myOrderDetail", new OrderDetailInfo());
        }
        model.addAttribute("listReciepts", orderSerive.getAllOrders());
        return "admin/recieptDetail";
    }

    @GetMapping("/account")
    public String Account(Model model) {
        model.addAttribute("accounts", accountService.getAllAccount());
        return "admin/account";
    }
}
