package org.o7planning.sbhibernateshoppingcart.controller;

import org.o7planning.sbhibernateshoppingcart.entities.Account;
import org.o7planning.sbhibernateshoppingcart.entities.Product;
import org.o7planning.sbhibernateshoppingcart.model.*;
import org.o7planning.sbhibernateshoppingcart.repository.AccountRepository;
import org.o7planning.sbhibernateshoppingcart.service.OrderDetailService;
import org.o7planning.sbhibernateshoppingcart.service.OrderSerive;
import org.o7planning.sbhibernateshoppingcart.service.ProductService;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {
    private final ProductService productService;

    private final AccountRepository accountRepository;

    private final OrderSerive orderSerive;

    public HomeController(ProductService productService, AccountRepository accountRepository, OrderSerive orderSerive, OrderDetailService orderDetailService) {
        this.productService = productService;
        this.accountRepository = accountRepository;
        this.orderSerive = orderSerive;
        this.orderDetailService = orderDetailService;
    }

    @GetMapping("/productList")
    public String product() {
        return "home/index";
    }

    private final OrderDetailService orderDetailService;

    @GetMapping("/")
    public String index(HttpSession httpSession, Model model) {
        if (httpSession.getAttribute("myCart") == null) {
            httpSession.setAttribute("myCart", new CartInfo());
        }
        model.addAttribute("listProduct", productService.getAll("Active"));
        return "home/index";
    }

    @GetMapping("/buyProduct/{code}")
    public String buyProduct(HttpSession session, @PathVariable Long code) {
        Product product = null;
        if (code != null) {
            product = productService.findProduct(code);
        }
        if (product != null) {
            CartInfo cartInfo = (CartInfo) session.getAttribute("myCart");
            ProductInfo productInfo = new ProductInfo(product);
            cartInfo.addProduct(productInfo, 1);
        }
        return "redirect:/";
    }

    @GetMapping("/myCart")
    public String addToCart(HttpSession session, Model model) {
        if (session.getAttribute("myCart") != null) {
            CartInfo myCart = (CartInfo) session.getAttribute("myCart");
            model.addAttribute("myCart", myCart);
        }
        return "home/cart";
    }

    @GetMapping("/removeProduct/{code}")
    public String removeProductInCart(HttpSession session, @PathVariable Long code) {
        Product product = null;
        if (code != null) {
            product = productService.findProduct(code);
        }
        if (product != null) {
            CartInfo cartInfo = (CartInfo) session.getAttribute("myCart");
            ProductInfo productInfo = new ProductInfo(product);
            cartInfo.removeProduct(productInfo);
        }
        return "redirect:/myCart";
    }

    @GetMapping("updateProduct/{code}/{type}")
    public String updateProductInCart(HttpSession session, @PathVariable Long code, @PathVariable String type) {
        CartInfo cartInfo = (CartInfo) session.getAttribute("myCart");
        int quantity = 0;
        if (type.equals("up")) {
            quantity = 1;
        } else if (type.equals("down")) {
            quantity = -1;
        }
        cartInfo.updateProduct(code, quantity);
        return "redirect:/myCart";
    }

    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})

    public String register(Model model, @ModelAttribute User user) {
        Account dbUser = accountRepository.findByUserName(user.getUsername());
        if (dbUser == null) {
            if (!user.getConfirmPassword().equals(user.getPassword())) {
                model.addAttribute("messages", "Mật khẩu không khớp !");
                return "admin/register";
            }
            Account account = Account.builder()
                    .userName(user.getUsername())
                    .encrytedPassword(new BCryptPasswordEncoder().encode(user.getPassword()))
                    .active(true)
                    .userRole(Account.ROLE_USER)
                    .build();
            Account account1 = accountRepository.save(account);
            System.out.println(account1);
            return "redirect:/login";
        } else {
            model.addAttribute("messages", "User Name đã được đăng ký, vui lòng kiểm tra lại !");
            return "admin/register";
        }
    }

    @GetMapping("/resgiter")
    public String signIn() {
        return "admin/register";
    }

    @GetMapping("/order")
    public String checkOrder(HttpSession session, Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountRepository.findByUserName(userDetails.getUsername());
        model.addAttribute("account", account);
        CartInfo cartInfo = (CartInfo) session.getAttribute("myCart");
        model.addAttribute("myCart", cartInfo);
        model.addAttribute("customerInfo", new CustomerInfo());
        return "home/order";
    }

    @PostMapping("order/comfirm")
    public String saveReciept(HttpSession httpSession, @ModelAttribute CustomerInfo customerInfo, Model model) throws Exception {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountRepository.findByUserName(userDetails.getUsername());
        CartInfo cartInfo = (CartInfo) httpSession.getAttribute("myCart");
        cartInfo.setCustomerInfo(customerInfo);
        model.addAttribute("order", orderSerive.save(cartInfo, account));
        httpSession.removeAttribute("myCart");
        return "home/congratulations";
    }

    @GetMapping("/reciept/{customerName}")
    public String Info(HttpSession httpSession, Model model, @PathVariable String customerName) {
        if (httpSession.getAttribute("myOrderDetail") == null) {
            httpSession.setAttribute("myOrderDetail", new OrderDetailInfo());
        }
        model.addAttribute("listRecieptsInUsers", orderSerive.findAllByUsername(customerName));
        return "home/reciept";
    }

    @GetMapping("/detailReciept/{id}")
    public String recieptDetail(HttpSession httpSession, Model model, @PathVariable Long id) {
        model.addAttribute("detailReciepts", orderDetailService.findOrders(id));
        if (httpSession.getAttribute("myOrderDetail") == null) {
            httpSession.setAttribute("myOrderDetail", new OrderDetailInfo());
        }
        model.addAttribute("listReciepts", orderSerive.getAllOrders());
        return "home/detailReciept";
    }
}
